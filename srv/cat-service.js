const cds = require('@sap/cds')

module.exports = (srv) => {

  const {Books} = cds.entities ('my.bookshop')
  const {Authors} = cds.entities ('my.bookshop')
  // Reduce stock of ordered books
  //srv.before ('CREATE', 'Orders', async (req) => {
    // const order = req.data
    // if (!order.amount || order.amount <= 0)  return req.error (400, 'Order at least 1 book')
    // const tx = cds.transaction(req)
    // const affectedRows = await tx.run (
    //   UPDATE (Books)
    //     .set   ({ stock: {'-=': order.amount}})
    //     .where ({ stock: {'>=': order.amount},/*and*/ ID: order.book_ID})
    // )
    // if (affectedRows === 0)  req.error (409, "Sold out, sorry")
  //})
 
  srv.on ('submitOrder',   async (req) => {
   const order = req.data
   console.log(order);
   const tx = cds.transaction(req)

  //  const books = await tx.run (SELECT.from(Books))
  //  console.log(books);
   const affectedRows = await tx.run (
      INSERT.into (Books) .columns (
        'ID', 'title', 'author_id', 'stock'
      ) .values ([
        order.id, order.title ,order.author, order.stock
      ])
 
   )

//    affectedRows2  = await tx.run (

//      UPDATE (Authors) .set ({name:  order.name })
//                         .where ({ID:555})
// )
//  console.log('bbb');
  })
  // Add some discount for overstocked books
  srv.after ('READ', 'Books', each => {
    if (each.stock > 111)  each.title += ' -- 11% discount!'
  })

  

  // Reduce stock of ordered books if available stock suffices
  // srv.on ('submitOrder', async req => {
    //  console.log("tt")
    // const {book,amount} = req.data
    // const n = await UPDATE (Books, book)
    //   .with ({ stock: {'-=': amount }})
    //   .where ({ stock: {'>=': amount }})
    // n > 0 || req.error (409,`${amount} exceeds stock for book #${book}`)
  // })

}